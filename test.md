
# Tests: Sigmoïde

## Couche cachées : 1

### 5 noeuds / couche

#### Epoch : 100

TEST 1 : 
```
=> Accuracy: 80%
Other stats:
  - true positives: 44% (35)
  - true negatives: 35% (28)
  - false positives: 11% (9)
  - false negatives: 9% (7)
  - false negatives: 9% (7)

Sensitivity: 83%
Specificity: 76%
```

TEST 2 : 
```
=> Accuracy: 78%
Other stats:
  - true positives: 41% (32)
  - true negatives: 38% (30)
  - false positives: 9% (7)
  - false negatives: 13% (10)
  - false negatives: 13% (10)

Sensitivity: 76%
Specificity: 81%
```

TEST 3 :
```
=> Accuracy: 76%
Other stats:
  - true positives: 43% (34)
  - true negatives: 33% (26)
  - false positives: 11% (9)
  - false negatives: 13% (10)
  - false negatives: 13% (10)

Sensitivity: 77%
Specificity: 74%
```

TEST 4 :
```
=> Accuracy: 78%
Other stats:
  - true positives: 47% (37)
  - true negatives: 32% (25)
  - false positives: 10% (8)
  - false negatives: 11% (9)
  - false negatives: 11% (9)

Sensitivity: 80%
Specificity: 76%
```

TEST 5 :
```
=> Accuracy: 77%
Other stats:
  - true positives: 46% (36)
  - true negatives: 32% (25)
  - false positives: 13% (10)
  - false negatives: 10% (8)
  - false negatives: 10% (8)

Sensitivity: 82%
Specificity: 71%
```

#### Epoch : 200

TEST 1 :
```
=> Accuracy: 81%
Other stats:
  - true positives: 39% (31)
  - true negatives: 42% (33)
  - false positives: 9% (7)
  - false negatives: 10% (8)
  - false negatives: 10% (8)

Sensitivity: 79%
Specificity: 82%
```

TEST 2 :
```
=> Accuracy: 80%
Other stats:
  - true positives: 44% (35)
  - true negatives: 35% (28)
  - false positives: 13% (10)
  - false negatives: 8% (6)
  - false negatives: 8% (6)

Sensitivity: 85%
Specificity: 74%
```

TEST 3 : 
```
=> Accuracy: 81%
Other stats:
  - true positives: 52% (41)
  - true negatives: 29% (23)
  - false positives: 10% (8)
  - false negatives: 9% (7)
  - false negatives: 9% (7)

Sensitivity: 85%
Specificity: 74%
```

TEST 4 : 
```
=> Accuracy: 78%
Other stats:
  - true positives: 57% (45)
  - true negatives: 22% (17)
  - false positives: 8% (6)
  - false negatives: 14% (11)
  - false negatives: 14% (11)

Sensitivity: 80%
Specificity: 74%
```

TEST 5 : 
```
=> Accuracy: 78%
Other stats:
  - true positives: 49% (39)
  - true negatives: 29% (23)
  - false positives: 13% (10)
  - false negatives: 9% (7)
  - false negatives: 9% (7)

Sensitivity: 85%
Specificity: 70%
```

#### Epoch : 500

TEST 1 :
```
=> Accuracy: 86%
Other stats:
  - true positives: 52% (41)
  - true negatives: 34% (27)
  - false positives: 6% (5)
  - false negatives: 8% (6)
  - false negatives: 8% (6)

Sensitivity: 87%
Specificity: 84%
```

TEST 2 :
```
=> Accuracy: 80%
Other stats:
  - true positives: 44% (35)
  - true negatives: 35% (28)
  - false positives: 8% (6)
  - false negatives: 13% (10)
  - false negatives: 13% (10)

Sensitivity: 78%
Specificity: 82%
```

TEST 3 :
```
=> Accuracy: 81%
Other stats:
  - true positives: 47% (37)
  - true negatives: 34% (27)
  - false positives: 9% (7)
  - false negatives: 10% (8)
  - false negatives: 10% (8)

Sensitivity: 82%
Specificity: 79%
```

TEST 4 :
```
=> Accuracy: 82%
Other stats:
  - true positives: 51% (40)
  - true negatives: 32% (25)
  - false positives: 5% (4)
  - false negatives: 13% (10)
  - false negatives: 13% (10)

Sensitivity: 80%
Specificity: 86%
```

TEST 5 :
```
=> Accuracy: 85%
Other stats:
  - true positives: 46% (36)
  - true negatives: 39% (31)
  - false positives: 9% (7)
  - false negatives: 6% (5)
  - false negatives: 6% (5)

Sensitivity: 88%
Specificity: 82%
```

### 10 noeuds / couche

#### Epoch : 100

TEST 1 : 
```
=> Accuracy: 80%
Other stats:
  - true positives: 46% (36)
  - true negatives: 34% (27)
  - false positives: 11% (9)
  - false negatives: 9% (7)
  - false negatives: 9% (7)

Sensitivity: 84%
Specificity: 75%
```

TEST 2 :
```
=> Accuracy: 78%
Other stats:
  - true positives: 48% (38)
  - true negatives: 30% (24)
  - false positives: 15% (12)
  - false negatives: 6% (5)
  - false negatives: 6% (5)

Sensitivity: 88%
Specificity: 67%
```

TEST 3 :
```
=> Accuracy: 85%
Other stats:
  - true positives: 51% (40)
  - true negatives: 34% (27)
  - false positives: 9% (7)
  - false negatives: 6% (5)
  - false negatives: 6% (5)

Sensitivity: 89%
Specificity: 79%
```

TEST 4 : 
```
=> Accuracy: 84%
Other stats:
  - true positives: 53% (42)
  - true negatives: 30% (24)
  - false positives: 6% (5)
  - false negatives: 10% (8)
  - false negatives: 10% (8)

Sensitivity: 84%
Specificity: 83%
```

TEST 5 : 
```
=> Accuracy: 75%
Other stats:
  - true positives: 47% (37)
  - true negatives: 28% (22)
  - false positives: 13% (10)
  - false negatives: 13% (10)
  - false negatives: 13% (10)

Sensitivity: 79%
Specificity: 69%
```

#### Epoch : 200
TEST 1 : 
```
=> Accuracy: 84%
Other stats:
  - true positives: 48% (38)
  - true negatives: 35% (28)
  - false positives: 4% (3)
  - false negatives: 13% (10)
  - false negatives: 13% (10)

Sensitivity: 79%
Specificity: 90%
```

TEST 2 : 
```
=> Accuracy: 86%
Other stats:
  - true positives: 47% (37)
  - true negatives: 39% (31)
  - false positives: 6% (5)
  - false negatives: 8% (6)
  - false negatives: 8% (6)

Sensitivity: 86%
Specificity: 86%
```

TEST 3 :
```
=> Accuracy: 85%
Other stats:
  - true positives: 46% (36)
  - true negatives: 39% (31)
  - false positives: 10% (8)
  - false negatives: 5% (4)
  - false negatives: 5% (4)

Sensitivity: 90%
Specificity: 79%
```

TEST 4 : 
```
=> Accuracy: 80%
Other stats:
  - true positives: 47% (37)
  - true negatives: 33% (26)
  - false positives: 6% (5)
  - false negatives: 14% (11)
  - false negatives: 14% (11)

Sensitivity: 77%
Specificity: 84%
```

TEST 5 : 
```
=> Accuracy: 80%
Other stats:
  - true positives: 48% (38)
  - true negatives: 32% (25)
  - false positives: 11% (9)
  - false negatives: 9% (7)
  - false negatives: 9% (7)

Sensitivity: 84%
Specificity: 74%
```

#### Epoch : 500

TEST 1 : 
```
=> Accuracy: 87%
Other stats:
  - true positives: 52% (41)
  - true negatives: 35% (28)
  - false positives: 4% (3)
  - false negatives: 9% (7)
  - false negatives: 9% (7)

Sensitivity: 85%
Specificity: 90%
```

TEST 2 :
```
=> Accuracy: 84%
Other stats:
  - true positives: 46% (36)
  - true negatives: 38% (30)
  - false positives: 10% (8)
  - false negatives: 6% (5)
  - false negatives: 6% (5)

Sensitivity: 88%
Specificity: 79%
```

TEST 3 :
```
=> Accuracy: 89%
Other stats:
  - true positives: 57% (45)
  - true negatives: 32% (25)
  - false positives: 5% (4)
  - false negatives: 6% (5)
  - false negatives: 6% (5)

Sensitivity: 90%
Specificity: 86%
```

TEST 4 : 
```
=> Accuracy: 85%
Other stats:
  - true positives: 48% (38)
  - true negatives: 37% (29)
  - false positives: 11% (9)
  - false negatives: 4% (3)
  - false negatives: 4% (3)

Sensitivity: 93%
Specificity: 76%
```

TEST 5 :
```
=> Accuracy: 84%
Other stats:
  - true positives: 43% (34)
  - true negatives: 41% (32)
  - false positives: 8% (6)
  - false negatives: 9% (7)
  - false negatives: 9% (7)

Sensitivity: 83%
Specificity: 84%
```

## Couche cachées : 2

### 5 noeuds / couche

#### Epoch : 100

TEST 1 :
```
=> Accuracy: 48%
Other stats:
  - true positives: 48% (38)
  - true negatives: 0% (0)
  - false positives: 52% (41)
  - false negatives: 0% (0)
  - false negatives: 0% (0)

Sensitivity: 100%
Specificity: 0%
```

TEST 2 : 
```
=> Accuracy: 48%
Other stats:
  - true positives: 18% (14)
  - true negatives: 30% (24)
  - false positives: 5% (4)
  - false negatives: 47% (37)
  - false negatives: 47% (37)

Sensitivity: 27%
Specificity: 86%
```

TEST 3 :
```
=> Accuracy: 54%
Other stats:
  - true positives: 54% (43)
  - true negatives: 0% (0)
  - false positives: 46% (36)
  - false negatives: 0% (0)
  - false negatives: 0% (0)

Sensitivity: 100%
Specificity: 0%
```

TEST 4 :
```
=> Accuracy: 47%
Other stats:
  - true positives: 47% (37)
  - true negatives: 0% (0)
  - false positives: 53% (42)
  - false negatives: 0% (0)
  - false negatives: 0% (0)

Sensitivity: 100%
Specificity: 0%
```

TEST 5 :
```
=> Accuracy: 54%
Other stats:
  - true positives: 54% (43)
  - true negatives: 0% (0)
  - false positives: 46% (36)
  - false negatives: 0% (0)
  - false negatives: 0% (0)

Sensitivity: 100%
Specificity: 0%
```

#### Epoch : 200
TEST 1 :
```
=> Accuracy: 82%
Other stats:
  - true positives: 51% (40)
  - true negatives: 32% (25)
  - false positives: 16% (13)
  - false negatives: 1% (1)
  - false negatives: 1% (1)

Sensitivity: 98%
Specificity: 66%
```

TEST 2 :
```
=> Accuracy: 53%
Other stats:
  - true positives: 53% (42)
  - true negatives: 0% (0)
  - false positives: 47% (37)
  - false negatives: 0% (0)
  - false negatives: 0% (0)

Sensitivity: 100%
Specificity: 0%
```

TEST 3 :
```
=> Accuracy: 66%
Other stats:
  - true positives: 38% (30)
  - true negatives: 28% (22)
  - false positives: 19% (15)
  - false negatives: 15% (12)
  - false negatives: 15% (12)

Sensitivity: 71%
Specificity: 59%
```

TEST 4 : 
```
=> Accuracy: 62%
Other stats:
  - true positives: 62% (49)
  - true negatives: 0% (0)
  - false positives: 38% (30)
  - false negatives: 0% (0)
  - false negatives: 0% (0)

Sensitivity: 100%
Specificity: 0%
```

TEST 5 :
```
=> Accuracy: 71%
Other stats:
  - true positives: 37% (29)
  - true negatives: 34% (27)
  - false positives: 9% (7)
  - false negatives: 20% (16)
  - false negatives: 20% (16)

Sensitivity: 64%
Specificity: 79%
```

#### Epoch : 500
TEST 1 :
```
=> Accuracy: 81%
Other stats:
  - true positives: 42% (33)
  - true negatives: 39% (31)
  - false positives: 11% (9)
  - false negatives: 8% (6)
  - false negatives: 8% (6)

Sensitivity: 85%
Specificity: 78%
```

TEST 2 :
```
=> Accuracy: 82%
Other stats:
  - true positives: 52% (41)
  - true negatives: 30% (24)
  - false positives: 14% (11)
  - false negatives: 4% (3)
  - false negatives: 4% (3)

Sensitivity: 93%
Specificity: 69%
```

TEST 3 : 
```
=> Accuracy: 84%
Other stats:
  - true positives: 49% (39)
  - true negatives: 34% (27)
  - false positives: 9% (7)
  - false negatives: 8% (6)
  - false negatives: 8% (6)

Sensitivity: 87%
Specificity: 79%
```

TEST 4 :
```
=> Accuracy: 91%
Other stats:
  - true positives: 49% (39)
  - true negatives: 42% (33)
  - false positives: 3% (2)
  - false negatives: 6% (5)
  - false negatives: 6% (5)

Sensitivity: 89%
Specificity: 94%
```

TEST 5 :
```
=> Accuracy: 81%
Other stats:
  - true positives: 43% (34)
  - true negatives: 38% (30)
  - false positives: 9% (7)
  - false negatives: 10% (8)
  - false negatives: 10% (8)

Sensitivity: 81%
Specificity: 81%
```

## Conclusion des tests : 

Il est clairement notable que le modèle avec une couche cachée , 10 unités sur la couche et 500 epochs d'entrainement nous offre les meilleures résultats. \
Le modèle avec 2 couche cachées, 5 unités d'entrainement et 100 epochs d'entrainement n'est pas viable. En effet les résultats sont aléatoires (en moyenne 50% d'accuracy). Cela est due au nombre d'Epochs qui n'est pas suffisant pour entrainer correctement le modèle avec sigmoïde. Le même problème se pose pour le même modèle avec 200 epochs. Les résultats sont en amélioration mais trop aléatoires et donc pas assez fiables.\

Il est intéressant de remarquer que le problème est lié à l'utilisation de sigmoïde car la convergence de l'erreur est relativement lente par rapport à tanh par exemple.

## Exemples de courbes





