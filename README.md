# Projet IA 2019 - Neural Network

## Utilisation

Le point d'entrée est le fichier main.py dont il est possible de régler plusieurs paramètres :

```
usage: main.py [-h] [-E [EPOCHS]] [--train [TRAIN]] [-b [BATCH]]
               [--show-error] [-v] [-a {sigmoid,tanh,relu}]
               hs [hs ...]
```

```python main.py -h``` permet d'afficher toutes les options et leur détail

ATTENTION: le programme nécessite Python 3.5 au minimum

## Exemples

```bash
# 1 couche cachée - 10 noeuds - tanh (500 époques, batchs de 4) - données mélangées auparavant
python main.py 10 --epoch 500 -b 4 -a tanh --show-error -v --shuffle

# 2 couches cachées - 5 noeuds par couche - sigmoid (500 époques, batchs de 1)
python main.py 5 5 --epoch 500 -b 1 -a sigmoid --show-error -v
```