---
title: Projet IA 2019
subtitle: Classification supervisée avec un réseau de neurones
author:
  - Paul HENG
  - Adrien OSSYWA
date: Avril 2019
titlepage: true
---

\newpage

# Utilisation

Les détails de l'utilisation du programme sont disponibles dans le ```README.md```. Il est possible de régler un grand nombre de paramètres lors du lancement du programme, comme :

* la taille des couches cachées (on peut en faire autant que l'on veut)
* le nombre d'epochs
* la taille des batch
* le nombre de données d'entraînement par rapport aux données totales
* la fonction d'activation
* la verbosité
* l'affichage des courbes d'erreur
* le mélange des données (avant séparation train/test)

# Données

1. On souhaite prédire l'attribut *target*
2. Il s'agit d'une classification binaire (soit la personne est malade, soit elle ne l'est pas)
3. Les attributs catégoriels sont :
    - ```sex```
    - ```chest_pain_type```
    - ```fasting_blood_sugar```
    - ```rest_ecg```
    - ```exercise_induced_angina```
    - ```st_slope```
    - ```thalassemia```

Il y a évidemment l'attribut ```target``` également, mais il s'agit de l'étiquette.

4. Il est possible de leur attributer une valeur entière numérique (ordinale), ou de les encoder sous forme de one-hot.

## Normalisation

1. On souhaite préserver la variance donc on va utiliser la standardisation de données avec la formule
$$
x' = \frac{x - \bar{x}}{\sigma}
$$
2. Il faut appliquer la normalisation avant la séparation des données.
Le modèle va être entraîné sur des données normalisées, les données de test doivent donc l'être également.
3. Les colonnes à normaliser sont les colonnes __0__,__3__,__4__,__7__,__9__ et __11__.

# Modèles de réseaux

Les *features* sont placées en **colonnes**.

## 1 couche cachée

### 5 unités

Soient les matrices d'entrée, cachée et sortie respectivement $A$, $H1$ et $O$, on a :

  - $A_{1,13}$
  - $W1_{13,5}$ et $B1_{1, 5}$
  - $H1_{1,5}$
  - $W2_{5, 2}$ et $B2_{1,2}$
  - $O_{1,2}$

![Réseau a une couche cachée (5 unités)](img/1_5.png){height="60%"}

### 10 unités

Soient les matrices d'entrée, cachée et sortie respectivement $A$, $H1$ et $O$, on a :

  - $A_{1,13}$
  - $W1_{13,10}$ et $B1_{1, 10}$
  - $H1_{1,10}$
  - $W2_{10, 2}$ et $B2_{1,2}$
  - $O_{1,2}$

![Réseau a une couche cachée (10 unités)](img/1_10.png){height="60%"}

## 2 couches cachées

### 5 unités

Soient les matrices d'entrée, cachées et sortie respectivement $A$, $H1$, $H2$ et $O$, on a :

  - $A_{1,13}$
  - $W1_{13,5}$ et $B1_{1, 5}$
  - $H1_{1,5}$
  - $W2_{5, 5}$ et $B2_{1,5}$
  - $H2_{1,5}$
  - $W3_{5, 2}$ et $B3_{1,2}$
  - $O_{1,2}$

![Réseau a deux couches cachées (5 unités)](img/2_5.png){height="60%"}

# Résultats

Une annexe est disponible avec quelques exemples d'exécutions.

Concrètement, on ne distingue pas vraiment de différence entre les modèles à 1 couche cachée.
En revanche, pour le modèle à 2 couches, selon la fonction d'activation, les résultats sont sensiblement différents. Ils sont en effet bien inférieurs en utilisant *sigmoïde* sur 100 ou 200 epochs. Cela est dû au fait que l'erreur d'entraînement converge plus lentement sur 2 couches avec *sigmoïde* qu'avec *tanh*.

Etant donné que nous avons un nombre très limité d'instances, il est donc difficile de déterminer quel modèle est effectivement le plus approprié.
Le modèle 1 couche, 10 noeuds sur 100 epochs semble être globalement le plus efficace, que ce soit en terme de précision ou de sensibilité (qui est a priori plus importante que la spécificité étant donné que les données concernent une maladie du coeur).
De manière générale, selon la vitesse de convergence de l'erreur, 500 epochs semble être un peu élevé car il y a un certain risque de sur-apprentissage.

Les exemples ci-dessous ont été réalisés en moyennant les résultats sur 10 exécutions de chaque modèle (données mélangées dans le .csv mais pas algorithmiquement au début du programme, par soucis de comparaison).
La commande utilisée est de la forme suivante :

```bash
# sans mélange
python test.py 5 -b 4 -a tanh

# avec mélange
python test.py 5 -b 4 -a sigmoid --shuffle
```

La valeur de l'argument importe peu car elle est réecrite par le script de test ```test.py```.

![Résultats avec sigmoïde (sans mélange)](img/tests_sigmoid.png){width="80%"}

![Résultats avec tanh (sans mélange)](img/tests_tanh.png){width="80%"}

![Résultats avec sigmoïde (mélange à chaque exécution)](img/tests_sigmoid2.png){width="80%"}

![Résultats avec tanh (mélange à chaque exécution)](img/tests_tanh2.png){width="80%"}

\newpage

# Exemples d'exécution

La courbe ci-dessous est obtenue pour 500 epochs avec une couche de 10 unités. La commande lancée ainsi que la sortie complète du programme sont également présentes.

![Courbes d'erreur pour une exécution](img/courbe_sigmoid.png){width="80%"}

```bash
# commande
python main.py -b 4 -a sigmoid --epoch 500 10 --show-error -v --shuffle

# sortie
Initializing 3-layers ANN with 500 epochs and batch size of 4.
Activation function: 'sigmoid'
Hidden layers: [10]
Data shuffled.
training...
Progress: |██████████████████████████████████████████████████| 100.0% Complete

testing model...

---
Correct: 70
Incorrect: 9
=> Accuracy: 88.61%
Other stats:
  - true positives: 51.9% (41)
  - true negatives: 36.71% (29)
  - false positives: 5.06% (4)
  - false negatives: 6.33% (5)

Sensitivity: 89.13%
Specificity: 87.88%
```

Même principe avec 2 couches de 5 unitées (tanh cette fois-ci).

![Courbes d'erreur pour une autre exécution](img/courbe_tanh.png){width="80%"}

```bash
# commande
python main.py -b 4 -a tanh --epoch 500 5 5 --show-error -v --shuffle

# sortie
Initializing 4-layers ANN with 500 epochs and batch size of 4.
Activation function: 'tanh'
Hidden layers: [5, 5]
Data shuffled.
training...
Progress: |██████████████████████████████████████████████████| 100.0% Complete

testing model...

---
Correct: 67
Incorrect: 12
=> Accuracy: 84.81%
Other stats:
  - true positives: 51.9% (41)
  - true negatives: 32.91% (26)
  - false positives: 6.33% (5)
  - false negatives: 8.86% (7)

Sensitivity: 85.42%
Specificity: 83.87%
```