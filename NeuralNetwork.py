import numpy as np
import Utils as utils
import pandas as pd
import matplotlib.pyplot as plt
import random

class NeuralNetwork:
  def __init__(self, data, hLayers, nbEpoch=100, batchSize=1, verbose=False, activation='sigmoid'):
    self.verbose = verbose
    self.data = data
    self.trainingData = None
    self.X_test = None
    self.Y_test = None
    self.maxEpoch = nbEpoch
    self.batchSize = batchSize
    self.err = 0.
    self.eta = 0.01
    self.X_train = np.ndarray((self.batchSize, self.data.shape[1]-1), float)
    self.Y_train = np.ndarray((self.batchSize, 2), float)
    if activation == 'sigmoid':
      self.activation = utils.sigmoid
    elif activation == 'tanh':
      self.activation = utils.tanh
    else:
      self.activation = utils.relu


    # tuple (weight, bias)
    self.layerParams = [None] * (len(hLayers) + 2)
    for i, item in enumerate([0, *hLayers, 2]):
      if i == 0:
        self.layerParams[i] = (None, None)
      elif i == 1:
        self.layerParams[i] = (
          np.random.rand(self.data.shape[1] - 1, item),
          np.zeros((1, item))
        )
      else:
        self.layerParams[i] = (
          np.random.rand(self.layerParams[i-1][0].shape[1], item),
          np.zeros((1, item))
        )

    # to draw a curve
    self.err_val = []
    self.err_idx = []
    self.test_err_val = []
    self.acc = []

  def shuffleData(self):
    """shuffles the data
    """
    np.random.shuffle(self.data)

  def shuffleTrainingData(self):
    """shuffles the training data
    """
    np.random.shuffle(self.trainingData)

  def splitTrainingAndTest(self, trainingSize):
    """splits the data in 2 sets : training and test
    
    Arguments:
        trainingSize {int} -- size of the training dataset
    """
    self.trainingData = self.data[:trainingSize]
    self.X_test = self.data[trainingSize:, :-1]
    self.Y_test = self.data[trainingSize:, -1:]

  def loadBatch(self, offset):
    """loads a batch in X_train and Y_train
    
    Arguments:
        offset {int} -- first index of the batch in the training dataset
    """
    self.X_train = self.trainingData[offset:offset+self.batchSize, :-1]
    tmp = self.trainingData[offset:offset+self.batchSize, -1:]
    for i in range(self.batchSize):
      self.Y_train[i] = utils.createOneHot(tmp[i][0], 2)

  def stardardize(self, cols=[]):
    avg = np.sum(self.data[:-1], axis=0) / self.data.shape[0]
    ecart = np.sqrt(np.var(self.data[:-1], axis=0))

    for i in range(self.data.shape[0]):
      for j in cols: # non categorical features
        self.data[i][j] = (self.data[i][j] - avg[j]) / ecart[j]

  def trainingEpoch(self):
    """do a complete epoch (feedforward, error, backpropagation)
    """

    self.shuffleTrainingData()
    seenTrainingData = 0
    self.err = 0
    while seenTrainingData < self.trainingData.shape[0]:
      self.loadBatch(seenTrainingData)

      Z = [None] * len(self.layerParams)
      A = Z.copy()

      deltaL = Z.copy()
      deltaW = Z.copy()
      deltaB = Z.copy()

      A[0] = self.X_train
      for i in range(1, len(Z)): # compute each hidden layer
        #* Feedforward
        # print(A[i-1].shape, self.layerParams[i][0].shape, self.layerParams[i][1].shape)
        Z[i] = np.add(A[i-1] @ self.layerParams[i][0], self.layerParams[i][1])
        if i < len(Z) - 1:
          A[i] = self.activation(Z[i])
        else:
          A[i] = utils.softmax(Z[i])

      #* Error computing
      self.err += utils.crossEntropy(A[-1], self.Y_train)

      #* Backpropagation

      #! compute delta for output layer [n]
      deltaL[-1] = A[-1] - self.Y_train
      deltaW[-1] = (A[-2].T @ deltaL[-1]) / self.batchSize
      deltaB[-1] = np.mean(deltaL[-1], axis=0, keepdims=True)

      #! compute delta for hidden layers [n-1, .., 1]
      for l in range(len(deltaL)-2, 0, -1):
        deltaL[l] = (deltaL[l+1] @ self.layerParams[l+1][0].T) * self.activation(Z[l], True)
        deltaW[l] = (A[l-1].T @ deltaL[l]) / self.batchSize
        deltaB[l] = np.mean(deltaL[l], axis=0, keepdims=True)

      #* Update parameters
      for l in range(len(self.layerParams) -1, 0, -1):
        _new = (
          self.layerParams[l][0] - (self.eta * deltaW[l]),
          self.layerParams[l][1] - (self.eta * deltaB[l])
        )
        self.layerParams[l] = _new


      seenTrainingData += self.batchSize

    # average the global error of the epoch
    self.err /= (self.trainingData.shape[0]/self.batchSize)



  def train(self):
    """trains the model
    """
    if self.verbose:
      print('training...')
      utils.printProgressBar(0, self.maxEpoch, prefix = 'Progress:', suffix = 'Complete', length = 50)
    for i in range(self.maxEpoch):
      if i > 0:
        self.err_val.append(self.err)
        self.err_idx.append(i)
      self.trainingEpoch()
      if self.verbose:
        utils.printProgressBar(i + 1, self.maxEpoch, prefix = 'Progress:', suffix = 'Complete', length = 50)
      if i == self.maxEpoch - 1:
        res = self.testModel(last=True)
      elif i > 0:
        res= self.testModel()
    return res

  def drawError(self):
    """draw the error curve
    """

    df = pd.DataFrame({
      "trainingError": self.err_val,
      "testingError": self.test_err_val
      },
      index=self.err_idx
    )
    df.to_csv('results.csv', ';', header=True)
    plt.rcParams["figure.figsize"] = [10, 7]
    lines = df.plot.line()
    plt.xlabel("Number of epochs")
    plt.ylabel("Cross-Entropy Loss")
    plt.yticks(np.arange(0, 1, step=0.05))
    plt.show()

  def testModel(self, last=False):
    """feedforward step for test data
    """
    if self.verbose and last:
      print('\ntesting model...')
    dataSize = len(self.X_test)
    err = 0
    #* Feedforward
    truePredictions = 0
    falsePredictions = 0
    truePositives = 0
    trueNegatives = 0
    falsePositives = 0
    falseNegatives = 0

    Z = [None] * len(self.layerParams)
    A = Z.copy()
    for k in range(dataSize):
      A[0] = self.X_test[k]
      #! only feedforward
      for l in range(1, len(Z)):
        Z[l] = np.add(A[l-1] @ self.layerParams[l][0], self.layerParams[l][1])
        if l < len(Z) - 1:
          A[l] = self.activation(Z[l])
        else:
          A[l] = utils.softmax(Z[l])

      Y = np.array([utils.createOneHot(self.Y_test[k][0], 2, 0)])
      err += utils.crossEntropy(A[-1], Y)
      #* Error computing
      if A[-1][0][0] > A[-1][0][1]:
        out = 0.
      else:
        out = 1.
      if out == self.Y_test[k][0]:
        truePredictions += 1
        if out == 0:
          trueNegatives += 1
        else:
          truePositives += 1
      else:
        falsePredictions += 1
        if out == 0:
          falseNegatives += 1
        else:
          falsePositives += 1

      accuracy = round((truePredictions/dataSize) * 100, 2)

    self.test_err_val.append(err/dataSize)
    self.acc.append(round((truePredictions/dataSize) * 100, 2))

    if last:
      sensitivity = round((truePositives/(truePositives+falseNegatives)) * 100, 2)
      specificity = round((trueNegatives/(trueNegatives+falsePositives)) * 100, 2)
      print((
        f'\n---\n'
        f'Correct: {truePredictions!r}\n'
        f'Incorrect: {falsePredictions!r}'
      ))
      print(f'=> Accuracy: {accuracy!r}%')
      print((f'Other stats:\n'
        f'  - true positives: {round((truePositives/dataSize) * 100, 2)!r}% ({truePositives})\n'
        f'  - true negatives: {round((trueNegatives/dataSize) * 100, 2)!r}% ({trueNegatives})\n'
        f'  - false positives: {round((falsePositives/dataSize) * 100, 2)!r}% ({falsePositives})\n'
        f'  - false negatives: {round((falseNegatives/dataSize) * 100, 2)!r}% ({falseNegatives})\n'
      ))
      print((
        f'Sensitivity: {sensitivity!r}%\n'
        f'Specificity: {specificity!r}%\n'
      ))
      return (sensitivity, specificity)
    return (None, None)
