from NeuralNetwork import *
import sys
import argparse

def main(**kwargs):
  parser = argparse.ArgumentParser(description='Trains and test a neural network.')
  parser.add_argument(
    'hs',
    nargs='+',
    help='hidden layers sizes'
  )
  parser.add_argument('-E', '--epochs',
    nargs='?',
    default=100,
    type=int,
    help='number of training epochs (default: 100)'
  )
  parser.add_argument('--train',
    nargs='?',
    default=224,
    type=int,
    help='number of training data (default: 224)'
  )
  parser.add_argument('-b', '--batch',
    nargs='?',
    default=1,
    type=int,
    help='number of elements in a mini-batch (default: 1)'
  )
  parser.add_argument(
    '--show-error',
    action='store_true',
    help='show the error curve (with matplot)'
  )
  parser.add_argument(
    '-v',
    '--verbose',
    action='store_true',
    help='enable verbose (default: False)'
  )
  parser.add_argument(
    '-a',
    '--activation',
    type=str,
    default='sigmoid',
    choices=['sigmoid', 'tanh', 'relu'],
    help='activation function for hidden layers (default: sigmoid)'
  )
  parser.add_argument(
    '--shuffle',
    action='store_true',
    help='shuffle all the data before execution (default: False)'
  )
  args = parser.parse_args()

  if args.train % args.batch != 0:
    raise Exception('batch size does not divide training data size')

  layers = kwargs['layers'] if 'layers' in kwargs.keys() else [int(i) for i in args.hs]
  epochs = kwargs['epochs'] if 'epochs' in kwargs.keys() else args.epochs
  batch = kwargs['batch'] if 'batch' in kwargs.keys() else args.batch
  print(f'Initializing {len(layers)+2}-layers ANN with {epochs!r} epochs and batch size of {batch!r}.')
  print(f'Activation function: {args.activation!r}')
  print(f'Hidden layers: {layers!r}')
  n = NeuralNetwork(utils.parseData('heart_disease_dataset.csv', ';', 13),
    layers,
    epochs,
    batch,
    args.verbose,
    args.activation
  )
  if args.shuffle:
    print('Data shuffled.')
    n.shuffleData()
  n.stardardize([0,3,4,7,9,11])
  n.splitTrainingAndTest(args.train)
  sens, spec = n.train()
  bestAccIdx = n.acc.index(max(n.acc))
  print(f'Best accuracy: {n.acc[bestAccIdx]!r}% on epoch {bestAccIdx + 1}')
  if args.show_error:
    n.drawError()
  return (n.acc[-1], sens, spec)

if __name__ == "__main__":
  main()