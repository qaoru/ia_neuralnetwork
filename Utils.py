import csv
import numpy as np
import math

def parseData(filepath : str, sep : str, hasHeaders=True) -> np.ndarray:
  """parses data from a csv
  
  Arguments:
    filepath {string} -- csv file path
    sep {string} -- csv separator
  
  Keyword Arguments:
    hasHeaders {bool} -- indicates whether the csv has headers (default: {True})
  
  Returns:
    numpy.ndarray -- 2D matrix containing the data
  """

  data = []
  with open(filepath, 'rt') as csvfile:
    reader = csv.reader(csvfile, delimiter=sep)
    if hasHeaders:
      next(reader)
    for row in reader:
      # skip if empty line
      if row[0] == '':
        continue

      data.append([float(i) for i in row])
    
  return np.array(data, float)


### Activation Functions ###

def sigmoid(x : np.ndarray, derivative=False):
  '''
  Compute the sigmoid value of x

  Parameters
  ----------
  x: value

  derivative: boolean, optional
    Indicates whether the derivative value shoud be returned instead or not

  Returns
  -------
  value
  '''
  sig = lambda x:1 / (1 + np.exp(-x))
  if derivative:
    return sig(x) * (1 - sig(x))
  return sig(x)

def tanh(x, derivative=False):
  '''
  Compute the tanh value of x

  Parameters
  ----------
  x: value
  
  derivative: boolean, optional
    Indicates whether the derivative value shoud be returned instead or not

  Returns
  -------
  value
  '''
  f = lambda x:np.tanh(x)
  if derivative:
    return 1.0 - f(x)**2
  return f(x)

def relu(x : np.ndarray, derivative=False):
  '''
  Compute the ReLU value of x

  Parameters
  ----------
  x: value
  
  derivative: boolean, optional
    Indicates whether the derivative value shoud be returned instead or not

  Returns
  -------
  value
  '''
  if derivative:
    return 1. * (x > 0)
  return x * (x > 0)

def softmax(m : np.ndarray):
  """applies softmax to a matrix
  
  Arguments:
    m {np.ndarray} -- matrix
  
  Returns:
    float -- softmax value
  """
  r = np.ndarray(m.shape)
  for i in range(m.shape[0]):
    for j in range(m.shape[1]):
      s = 0
      for c in range(m.shape[1]):
        s += np.exp(m[i][c])
      r[i][j] = np.exp(m[i][j]) / s
  return r

def createOneHot(label, size, start=0):
  '''creates a one-hot vector
  Arguments:
    label {int} -- label value
    size {int} -- size of vector (= number of classes)
  
  Keyword Arguments:
    start {int} -- first class (default: {0})
  
  Returns:
    np.ndarray(size, 1) -- one-hot vector
  '''

  onehot = []
  for i in range(start, size + start):
    if i == label:
      onehot.append(1.)
    else:
      onehot.append(0.)
  return np.asarray(onehot)


def crossEntropy(yHat: np.ndarray, y: np.ndarray):
  """computes the cross-entropy between 2 matrixes
  
  Arguments:
    yHat {np.ndarray} -- output matrix
    y {np.ndarray} -- labels matrix
  
  Returns:
    float -- cross-entropy error value
  """
  K = y.shape[1]
  batchSize = y.shape[0]
  cost = 0.
  for c in range(batchSize):
    for k in range(K):
      cost += y[c][k]*math.log(yHat[c][k])
  return - (1./batchSize) * cost

## snippet by Greenstick on StackOverflow (https://stackoverflow.com/a/34325723) ##
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total: 
        print()
