import main
import csv
import os

with open('out_tanh3.csv', 'a', newline='') as csvfile:
  fieldnames = ['layers', 'epochs', 'accuracy', 'sensitivity', 'specificity']
  writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
  writer.writeheader()

  for l in [[5], [10], [5,5]]:
    for e in [100, 200, 500]:
      acc, sens, spec = (0,0,0)
      for i in range(10):
        _acc, _sens, _spec = main.main(layers=l, epochs=e)
        acc += _acc
        sens += _sens
        spec += _spec

      writer.writerow({
        'layers': str(l),
        'epochs': str(e),
        'accuracy': acc/10,
        'sensitivity': sens/10,
        'specificity': spec/10
      })